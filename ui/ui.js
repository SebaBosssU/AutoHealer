define('two/autoHealer/ui', [
    'two/autoHealer',
    'two/FrontButton',
    'two/locale',
    'two/utils',
    'two/eventQueue'
], function (
    autoHealer,
    FrontButton,
    Locale,
    utils,
    eventQueue
) {
    var opener

    function HealerInterface () {
        Locale.create('healer', __healer_locale, 'pl')
        
        opener = new FrontButton(Locale('healer', 'title'), {
            classHover: false,
            classBlur: false,
            tooltip: Locale('healer', 'description')
        })

        opener.click(function () {
            if (autoHealer.isRunning()) {
                autoHealer.stop()
                utils.emitNotif('success', Locale('healer', 'deactivated'))
            } else {
                autoHealer.start()
                utils.emitNotif('success', Locale('healer', 'activated'))
            }
        })

        eventQueue.bind('Healer/started', function () {
            opener.$elem.removeClass('btn-green').addClass('btn-red')
        })

        eventQueue.bind('Healer/stopped', function () {
            opener.$elem.removeClass('btn-red').addClass('btn-green')
        })

        if (autoHealer.isRunning()) {
            eventQueue.trigger('Healer/started')
        }

        return opener
    }

    autoHealer.interface = function () {
        autoHealer.interface = HealerInterface()
    }
})
