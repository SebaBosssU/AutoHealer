require([
    'two/ready',
    'two/autoHealer',
    'Lockr',
    'two/eventQueue',
    'two/autoHealer/ui',
], function (
    ready,
    autoHealer,
    Lockr,
    eventQueue
) {
    if (autoHealer.isInitialized()) {
        return false
    }

    ready(function () {
        autoHealer.init()
        autoHealer.interface()
        
        ready(function () {
            if (Lockr.get('healer-active', false, true)) {
                autoHealer.start()
            }

            eventQueue.bind('Healer/started', function () {
                Lockr.set('healer-active', true)
            })

            eventQueue.bind('Healer/stopped', function () {
                Lockr.set('healer-active', false)
            })
        }, ['initial_village'])
    })
})
