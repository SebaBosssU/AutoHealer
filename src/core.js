define('two/autoHealer', [
    'two/eventQueue',
    'two/locale',
    'two/utils',
    'Lockr'
], function(
    eventQueue,
    Locale,
    utils,
    Lockr
) {
    var modelDataService = injector.get('modelDataService')
    var socketService = injector.get('socketService')
    var routeProvider = injector.get('routeProvider')
    var initialized = false
    var running = false
    var recall = true
    var interval = 3000
    var interval1 = 1000

    function healUnits() {
        var player = modelDataService.getSelectedCharacter()
        var villages = player.getVillageList()
        villages.forEach(function(village, index) {
            var hospital = village.hospital
            var patients = hospital.patients
            var healed = patients.healed
            var pending = patients.pending
            if (healed.length == 0) {
            } else {
                setTimeout(function() {
                    healed.forEach(function(heal, index) {
                        setTimeout(function() {
                            socketService.emit(routeProvider.HOSPITAL_RELEASE_PATIENT, {
                                village_id: village.getId(),
                                patient_id: heal.id
                            })
                        }, index * interval1)
                        utils.emitNotif('success', 'W wiosce: ' + village.getName() + ' wyleczono: ' + heal.id)
                    })
                }, index * interval)
            }
        })
        utils.emitNotif('success', Locale('healer', 'deactivated'))
        autoHealer.stop()
    }
    var autoHealer = {}
    autoHealer.init = function() {
        initialized = true
    }
    autoHealer.start = function() {
        eventQueue.trigger('Healer/started')
        running = true
        healUnits()
    }
    autoHealer.stop = function() {
        eventQueue.trigger('Healer/stopped')
        running = false
    }
    autoHealer.isRunning = function() {
        return running
    }
    autoHealer.isInitialized = function() {
        return initialized
    }
    return autoHealer
})